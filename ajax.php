<?php
	require_once "./includes/applist.php";
	$a = new Applist();
	session_start();

	$return = array();
	if (isset($_POST['search_string']))
		$return = $a->search($_POST['search_string']);
	else
		array_push($return, "nothing to see here");
	echo json_encode($return);
?>