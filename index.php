<?php
	require_once "./includes/applist.php";
	$a = new Applist();
	$a->init();
?>
<!doctype html>
<html lang='en-GB'>
<head>
	<meta charset='utf-8' />
	<title><?php echo APP_NAME; ?></title>
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<link href='http://fonts.googleapis.com/css?family=Luckiest+Guy' rel='stylesheet' type='text/css'>
</head>
<body>

	<div id='wrapper'>
		<header>
			<h1><a href='/'><?php echo APP_NAME; ?></a></h1>
			<nav class='categories'>
				<ul>
					<li><a href='/category/all/'>all</a></li>
<?php foreach ($a->categories as $category): ?>
					<li><a href='/category/<?php echo $category['category_id']; ?>/'><?php echo $category['category_name']; ?></a></li>
<?php endforeach; ?> 
				</ul>
			</nav>
		</header>

		<section class='app_list'>
<?php foreach ($a->apps as $app): ?>
			<article>
				<h2><a href='<?php echo $app['url']; ?>'><?php echo $app['name']; ?></a> (<?php echo $app['price'];?>)</h2>
				<small><a href='/category/<?php echo $app['category']; ?>/'><?php echo $a->lookup_category($app['category']); ?></a></small>
				<p><?php echo $app['description']; ?></p>
			</article>
<?php endforeach; ?>
		</section>

		<section>
			<nav class='pagination'>
				<ul>
<?php foreach ($a->pages as $page): ?>
					<li><a href='<?php echo $page['url']; ?>'><?php echo $page['number']; ?></a></li>
<?php endforeach; ?>
				</ul>
			</nav>
		</section>

		<footer>
			<form method='POST'>
				<fieldset>
					<legend>Search</legend>
					<input type='text' name='search_string' placeholder='search' />
					<input type='submit' name='submit_search' value='hurf' />
				</fieldset>
			</form>
		</footer>
	</div>

</body>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>
<script type='text/javascript' src='/includes/applist.js'></script>
</html>