<?php

require_once "./includes/applist-config.php";

class Applist {

	private $conn, $loc;
	public $per_page = 20;

	public function __construct() {
		try {
			$this->conn = new PDO('mysql:host='.DB_HOST.';port='.DB_PORT.';dbname='.DB_DB, DB_USER, DB_PASS);
		} catch (PDOException $e) {
			$this->error($e);
		}
	}


	/*
	* 	page_init
	*
	*	Initialises the current page, creates array of the relevant
	*	data to display
	*
	*	@since 0.1
	*/
	public function init() {
		session_start();
		$this->where_am_i();
		$this->apps = $this->get_apps();
		$this->pages = $this->get_pages();
		$this->categories = $this->get_categories();
		if (isset($_SESSION['per_page']))
			$this->per_page = $_SESSION['per_page'];
	}


	/*
	* 	where_am_i
	*
	*	Simple function to determine what category and page to serve up
	*	dependent on request uri
	*
	*	@since 0.1
	*/
	private function where_am_i() {
		$request = explode("/", $_SERVER['REQUEST_URI']);
		$page_type = (!empty($request[1])) ? $request[1] : 'page' ;
		if ($page_type == 'category') {
			$category = $request[2];
			$page = (isset($request[3])) ? $request[3] : 1;

		}
		else if ($page_type == 'page') {
			$category = 'all';
			$page = (!empty($request[2])) ? $request[2] : '1' ;
		}
		else {
			$category = 'all';
			$page = 1;
		}
		$this->loc = array("page" => $page, "category" => $category);
	}


	/*
	* 	current_page
	*
	*	Method to get the page number from the where_am_i result
	*
	*	@since 0.1
	*	@return 	page 	returns the current page number
	*/
	private function current_page() {
		return $this->loc['page'];
	}


	/*
	* 	current_category
	*
	*	Method to get the category number from the where_am_i result
	*
	*	@since 0.1
	*	@return 	category 	returns the current category number
	*/
	private function current_category() {
		return $this->loc['category'];
	}


	/*
	*	search
	*
	*	Searches the database for the supplied search term
	*
	*	@since 		0.1
	*	@param 		search_term	Term to search for
	*	@return 	return 		Array of search results
	*/
	public function search($search_term) {
		$return = array();
		if (empty($search_term)) {
			$return = array("Search term empty.");
		}
		else {
			$sql = "SELECT id, name, url, description, price, category, karma
					FROM application
					WHERE name LIKE'%"."$search_term"."%'
					ORDER BY karma
					LIMIT ".$this->per_page.";";
			$query = $this->conn->prepare($sql);
			//$query->bindParam(":query", $search_term, PDO::PARAM_STR);
			$query->execute();
			if (!$query->rowCount())
				$return = array('No results found.');
			else
				foreach($query->fetchAll() as $row)
					array_push($return, $row);
			return $return;
		}
	}


	/*
	*	get_apps
	*
	*	Returns an associative array of applications
	*
	*	@since 		0.1
	*	@param 		page 	Current page to load
	*	@return 	apps 	Associative array of applications
	*/
	private function get_apps() {
		$page = $this->current_page();
		$category = $this->current_category();
		$offset = ($page * $this->per_page) - $this->per_page;
		if ($offset < 0)
			$offset = 0;
		if ($category != 'all')
			$sql = 'SELECT id, name, url, description, price, category, karma
					FROM application
					WHERE category = :category
					ORDER BY karma DESC
					LIMIT '.$this->per_page.' OFFSET :offset ';
		else
			$sql = 'SELECT id, name, url, description, price, category, karma
					FROM application
					ORDER BY karma DESC
					LIMIT '.$this->per_page.' OFFSET :offset ';

		try {
			$query = $this->conn->prepare($sql);
			if ($category != 'all')
				$query->bindParam(':category', $category, PDO::PARAM_INT);
			$query->bindParam(':offset', $offset, PDO::PARAM_INT);
			$query->execute();
			$result = $query->fetchAll();
		} catch (PDOException $e) {
			$this->error($e);
		}
		return $result;
	}


	/*
	*	get_pages
	*
	*	Returns an array of page numbers
	*
	*	@since 		0.1
	*	@param 		$type 	Pages only associated to a certain category
	*	@return 	return 	Array of pages
	*/
	private function get_pages() {
		$category = $this->current_category();
		if ($category != 'all')
			$sql = "SELECT id
					FROM application
					WHERE category = :category ";
		else
			$sql = "SELECT id
					FROM application;";
		$query = $this->conn->prepare($sql);
		if ($category != 'all')
			$query->bindParam(":category", $category, PDO::PARAM_INT);
		$query->execute();
		$return = array();
		$pages = ceil($query->rowCount() / $this->per_page);
		if ($category != 'all')
			for ($i = 0; $i < $pages; $i++)
				array_push($return, array("number" => $i+1, "url" => "/category/$category/".($i+1)."/"));
		else
			for ($i = 0; $i < $pages; $i++)
				array_push($return, array("number" => $i+1, "url" => "/page/".($i+1)."/"));
		return $return;
	}


	private function get_categories() {
		$sql = "SELECT category_id, category_name
				FROM categories";
		$query = $this->conn->prepare($sql);
		$query->execute();
		$categories = $query->fetchAll();
		return $categories;
	}


	/*
	*	lookup_category
	*
	*	Looks up a category name by category id
	*
	*	@since 		0.1
	*	@return 	category	String of the category searched
	*/
	public function lookup_category($category_id) {
		$sql = "SELECT category_name
				FROM categories
				WHERE category_id = :id
				LIMIT 1;";
		$query = $this->conn->prepare($sql);
		$query->execute(array(':id' => $category_id));
		$category = $query->fetchColumn(0);
		return $category;
	}


	/*
	*	error
	*
	*	Basic error handling, so it's all nicely formatted and consistent
	*
	*	@since 		0.1
	*/
	public function error($str) {
		print("<p class='app_error'><strong>Error:</strong> ".$str."</p>");
	}

}

?>