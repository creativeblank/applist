$(document).ready(function() {

	$('input[name=submit_search]').click(function() {
		$.ajax({
			type: 'POST',
			url: './ajax.php',
			dataType: 'json', 
			data: {
				search_string: $('input[name=search_string]').val()
			},
			success: function(data) {
				$('.app_list').animate({
					opacity: '0'
				}, 500, 'linear', function() {
					$('.app_list').html('');
					$.each(data, function(index, value) {
						$('.app_list').html('			<article> \
				<h2><a href="link">name</a> (price)</h2> \
				<small><a href="link">category</a></small> \
				<p>description</p> \
			</article>');
					});
					$('.app_list').animate({opacity:'1'},1000);
				});
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus + " " + errorThrown)
			}
		});
		return false;
	});

});