<?php
	// Title of the application / site
	define('APP_NAME', 'herp de derp');
	// Database host
	define('DB_HOST', 'localhost');
	// Database port
	define('DB_PORT', '8889');
	// Database username
	define('DB_USER', 'root');
	// Database password
	define('DB_PASS', 'root');
	// Database, er, database. Yeah.
	define('DB_DB', 'app_list');
	// application / site version
	define('VERSION', '0.1');
?>